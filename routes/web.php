<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('products-list', [\App\Http\Controllers\ProductsController::class, 'index'])->name('products');

Route::get('products/{product}', [\App\Http\Controllers\ProductsController::class, 'show'])->name('products.show');

Route::get('products/{product}/images', [\App\Http\Controllers\ProductsController::class, 'galleryDisplay'])->name('products.images');

Route::get('categories', [\App\Http\Controllers\CategoriesController::class, 'index'])->name('categories');

Route::get('categories/{category}', [\App\Http\Controllers\CategoriesController::class, 'show'])->name('categories.show');

Route::delete('ajax/product-image/{image_id}', [\App\Http\Controllers\ProductImageController::class, 'destroy'])->name('ajax.products.image.delete');

//User profile routes
Route::namespace('User')->prefix('user')->name('user.')->middleware(['auth'])->group(function() {
    Route::get('profile', [\App\Http\Controllers\User\UserAccountController::class, 'index'])->name('profile');
    Route::get('{user}/edit', [\App\Http\Controllers\User\UserAccountController::class, 'edit'])->middleware('can:update,user')->name('edit');
    Route::put('{user}/update', [\App\Http\Controllers\User\UserAccountController::class, 'update'])->middleware('can:update,user')->name('update');

    Route::get('wishlist', [\App\Http\Controllers\User\WishlistController::class, 'index'])->name('wishlist');
});

//Admin routes
Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware(['auth', 'admin'])->group(function() {

    Route::get('/', [\App\Http\Controllers\Admin\ControlPanelController::class, '__invoke'])->name('home');

    Route::name('products')->group(function(){
        Route::get('products', [\App\Http\Controllers\Admin\ProductsController::class, 'index']);
        Route::get('products/create', [\App\Http\Controllers\Admin\ProductsController::class, 'create'])->name('.create');
        Route::post('products', [\App\Http\Controllers\Admin\ProductsController::class, 'store'])->name('.store');
        Route::get('products/{product}/edit', [\App\Http\Controllers\Admin\ProductsController::class, 'edit'])->name('.edit');
        Route::put('products/{product}/update', [\App\Http\Controllers\Admin\ProductsController::class, 'update'])->name('.update');
        Route::delete('products/{product}', [\App\Http\Controllers\Admin\ProductsController::class, 'destroy'])->name('.delete');
    });

    Route::name('categories')->group(function() {
        Route::get('categories', [\App\Http\Controllers\Admin\CategoriesController::class, 'index']);
        Route::get('categories/create', [\App\Http\Controllers\Admin\CategoriesController::class, 'create'])->name('.create');
        Route::post('categories', [\App\Http\Controllers\Admin\CategoriesController::class, 'store'])->name('.store');
        Route::get('categories/{category}/edit', [\App\Http\Controllers\Admin\CategoriesController::class, 'edit'])->name('.edit');
        Route::put('categories/{category}/update', [\App\Http\Controllers\Admin\CategoriesController::class, 'update'])->name('.update');
        Route::delete('categories/{category}', [\App\Http\Controllers\Admin\CategoriesController::class, 'destroy'])->name('.delete');
    });

    Route::name('users')->group(function() {
        Route::get('users', [\App\Http\Controllers\Admin\UsersController::class, 'index']);
        Route::get('users/{user}', [\App\Http\Controllers\Admin\UsersController::class, 'show'])->name('users.show');
        Route::delete('users/{user}', [\App\Http\Controllers\Admin\UsersController::class, 'destroy'])->name('.delete');
    });

    Route::name('orders')->group(function () {
        Route::get('orders', [\App\Http\Controllers\Admin\OrdersController::class, 'index']);
        Route::get('orders/{order}/edit', [\App\Http\Controllers\Admin\OrdersController::class, 'edit'])->name('.edit');
        Route::delete('orders/{order}', [\App\Http\Controllers\Admin\OrdersController::class, 'destroy'])->name('.delete');
    });
});
//Shopping cart routing
Route::middleware('auth')->group(function () {
    Route::get('cart', [\App\Http\Controllers\CartController::class, 'index'])->name('cart');
    Route::post('cart/{product}/add', [\App\Http\Controllers\CartController::class, 'add'] )->name('cart.add');
    Route::post('cart/{product}/count/update', [\App\Http\Controllers\CartController::class, 'countUpdate'])->name('cart.count.update');
    Route::post('cart/product/delete', [\App\Http\Controllers\CartController::class, 'delete'])->name('cart.delete');

    Route::get('checkout', [\App\Http\Controllers\CheckoutController::class, '__invoke'])->name('checkout');
    Route::post('order', [\App\Http\Controllers\OrdersController::class, 'store'])->name('order.create');

    //Wishlist routing
    Route::get('wishlist/{product}/add', [\App\Http\Controllers\WishListController::class, 'add'])->name('wishlist.add');
    Route::delete('wishlist/{product}/delete', [\App\Http\Controllers\WishListController::class, 'delete'])->name('wishlist.delete');
});



