@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 class="text-center">{{ __('Create new category') }}</h3>
                <hr>
            </div>
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-12 bg-light">
                <form action="{{ route('admin.categories.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputName"><strong>{{__('Name: ')}}</strong></label>
                                <input id="inputName"
                                       type="text"
                                       class="form-control @error('name') is-invalid @enderror "
                                       name="name"
                                       value=""
                                       autocomplete="name"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputDescription"><strong>{{__('Description: ')}}</strong></label>
                                <textarea id="inputDescription"
                                          class="form-control @error('description') is-invalid @enderror "
                                          name="description"
                                          cols="30"
                                          rows="10">
                                    </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-success">Add Category</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
