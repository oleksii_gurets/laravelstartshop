@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 class="text-center">{{ __('Product categories list') }}</h3>
                <hr>
            </div>
            <div class="col-md-12">
                <table class="table align-self-center">
                    <thead class="text-center thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Category name</th>
                        <th scope="col">Category description</th>
                        <th scope="col">Products quantity</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody class="table-secondary">
                    @foreach( $categories as $category)
                        <tr>
                            <td class="text-center">{{ $category->id }}</td>
                            <td class="text-center">{{ $category->name }}</td>
                            <td class="text-center">{{ $category->description }}</td>
                            <td class="text-center">{{ $category->products->count() }}</td>
                            <td class="text-center">
                                <a href="{{ route('categories.show', $category) }}" class="btn btn-outline-secondary">View</a>
                                <a href="{{ route('admin.categories.edit', $category) }}" class="btn btn-outline-success">Edit</a>
                                <form action="{{ route('admin.categories.delete', $category) }}" class="d-inline-block " method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-outline-danger" value="Remove" >
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-2">
                {{ $categories->links() }}
            </div>
        </div>
    </div>
@endsection
