@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 class="text-center">{{ __('Orders list') }}</h3>
                <hr>
            </div>
            <div class="col-md-12">
                <table class="table align-self-center">
                    <thead class="text-center thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">User</th>
                        <th scope="col">Email</th>
                        <th scope="col">Status</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Total price</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody class="table-secondary">
                    @foreach( $orders as $order)
                        <tr>
                            <td class="text-center">{{ $order->id }}</td>
                            <td class="text-center">{{ $order->name }}</td>
                            <td class="text-center">{{ $order->email }}</td>
                            <td class="text-center">{{ $order->status->name }}</td>
                            <td class="text-center">{{ $order->created_at }}</td>
                            <td class="text-center">{{ $order->total }}</td>
                            <td class="text-center">
                                <a href="{{ route('admin.orders.edit', $order) }}" class="btn btn-outline-success">Edit</a>
                                <form action="{{ route('admin.orders.delete', $order) }}" class="d-inline-block " method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-outline-danger" value="Remove" >
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-2">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection
