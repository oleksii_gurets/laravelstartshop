@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 class="text-center">{{ __('Edit product: ') }} {{__($product->title)}}</h3>
                <hr>
            </div>
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-12 bg-light">
                <form action="{{ route('admin.products.update', $product) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputTitle"><strong>{{__('Title: ')}}</strong></label>
                                <input id="inputTitle"
                                       type="text"
                                       class="form-control @error('title') is-invalid @enderror "
                                       name="title"
                                       value="{{ $product->title }}"
                                       autocomplete="title"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputDescription"><strong>{{__('Description: ')}}</strong></label>
                                <textarea id="inputDescription"
                                          class="form-control @error('description') is-invalid @enderror "
                                          name="description"
                                          cols="30"
                                          rows="10">
                                    {{ $product->description }}
                                    </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputShortDescription"><strong>{{__('Short description: ')}}</strong></label>
                                <textarea id="inputShortDescription"
                                          class="form-control @error('short_description') is-invalid @enderror "
                                          name="short_description"
                                          cols="30"
                                          rows="10">
                                    {{ $product->short_description }}
                                        </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputSKU"><strong>{{__('SKU: ')}}</strong></label>
                                <input id="inputSKU"
                                       type="text"
                                       class="form-control @error('SKU') is-invalid @enderror "
                                       name="SKU"
                                       value="{{ $product->SKU }}"
                                       autocomplete="SKU"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputPrice"><strong>{{__('Price: ')}}</strong></label>
                                <input id="inputPrice"
                                       type="text"
                                       class="form-control @error('price') is-invalid @enderror "
                                       name="price"
                                       value="{{ $product->price }}"
                                       autocomplete="price"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputDiscount"><strong>{{__('Discount: ')}}</strong></label>
                                <input id="inputDiscount"
                                       type="number"
                                       class="form-control @error('discount') is-invalid @enderror "
                                       name="discount"
                                       value="{{ $product->discount }}"
                                       autocomplete="discount"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputIn_stock"><strong>{{__('In stock (quantity): ')}}</strong></label>
                                <input id="inputIn_stock"
                                       type="number"
                                       class="form-control @error('in_stock') is-invalid @enderror "
                                       name="in_stock"
                                       value="{{ $product->in_stock }}"
                                       autocomplete="in_stock"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="category"><strong>{{__('Category: ')}}</strong></label>
                                <select name="category_id"
                                        id="category"
                                        class="form-control @error('category') is-invalid @enderror ">
                                    @foreach($categories as $category)
                                        <option value="{{ $category['id'] }}"
                                        {{ $category['id'] === $product->category->id ? 'selected' : '' }}>
                                            {{  $category['name'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="thumbnail" class="col-md-4-left"><strong>{{__('Thumbnail: ')}}</strong></label>
                                <div class="col-md-6">
                                    @if(Storage::has($product->thumbnail))
                                        <img src="{{ Storage::url($product->thumbnail) }}" width="75" height="95" alt="{{ $product->title }}">
                                    @else
                                        <img src="{{ $product->thumbnail }}" width="75" height="95" alt="{{ $product->title }}">
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <input id="thumbnail" type="file" name="thumbnail">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="images" class="col-md-4-left"><strong>{{__('Images: ')}}</strong></label>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            @foreach($product->gallery as $image)
                                                @if(Storage::has($image->path))
                                                    <div class="col-sm-12 d-flex justify-content-center align-items-center">
                                                        <img src="{{ Storage::url($image->path) }}" width="75" height="95" alt="">
                                                        <a data-image_id="{{ $image->id }}"
                                                            data-route="{{ route('ajax.products.image.delete', $image->id) }}"
                                                           class="btn btn-danger remove-product-image">
                                                            X
                                                        </a>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <input id="images" type="file" name="images[]" multiple>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-success">Edit Product</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <script src="{{ asset('js/product-images.js') }}" type="text/javascript" ></script>
@endsection
