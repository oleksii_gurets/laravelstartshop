@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 class="text-center">{{ __('Create new product item') }}</h3>
                <hr>
            </div>
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-12 bg-light">
                <form action="{{ route('admin.products.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputTitle"><strong>{{__('Title: ')}}</strong></label>
                                <input id="inputTitle"
                                       type="text"
                                       class="form-control @error('title') is-invalid @enderror "
                                       name="title"
                                       value="{{ old('title') }}"
                                       autocomplete="title"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputDescription"><strong>{{__('Description: ')}}</strong></label>
                                <textarea id="inputDescription"
                                          class="form-control @error('description') is-invalid @enderror "
                                          name="description"
                                          value="{{ old('description') }}"
                                          cols="30"
                                          rows="10">
                                    </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputShortDescription"><strong>{{__('Short description: ')}}</strong></label>
                                <textarea id="inputShortDescription"
                                          class="form-control @error('short_description') is-invalid @enderror "
                                          name="short_description"
                                          value="{{ old('short_description') }}"
                                          cols="30"
                                          rows="10">
                                        </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputSKU"><strong>{{__('SKU: ')}}</strong></label>
                                <input id="inputSKU"
                                       type="text"
                                       class="form-control @error('SKU') is-invalid @enderror "
                                       name="SKU"
                                       value="{{ old('SKU') }}"
                                       autocomplete="SKU"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputPrice"><strong>{{__('Price: ')}}</strong></label>
                                <input id="inputPrice"
                                       type="text"
                                       class="form-control @error('price') is-invalid @enderror "
                                       name="price"
                                       value="{{ old('price') }}"
                                       autocomplete="price"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputDiscount"><strong>{{__('Discount: ')}}</strong></label>
                                <input id="inputDiscount"
                                       type="number"
                                       class="form-control @error('discount') is-invalid @enderror "
                                       name="discount"
                                       value="{{ old('discount') }}"
                                       autocomplete="discount"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="inputIn_stock"><strong>{{__('In stock (quantity): ')}}</strong></label>
                                <input id="inputIn_stock"
                                       type="number"
                                       class="form-control @error('in_stock') is-invalid @enderror "
                                       name="in_stock"
                                       value="{{ old('in_stock') }}"
                                       autocomplete="in_stock"
                                       autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="category"><strong>{{__('Categories: ')}}</strong></label>
                                <select name="category"
                                        id="categories"
                                        class="form-control @error('category') is-invalid @enderror ">
                                    @foreach($categories as $category)
                                    <option value="{{ $category['id'] }}">
                                        {{  $category['name'] }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="thumbnail" class="col-md-4-left"><strong>{{__('Thumbnail: ')}}</strong></label>
                                <input id="thumbnail" type="file" name="thumbnail">
                            </div>
                            <div class="col-md-6">
                                <img src="#" id="thumbnail-preview" alt="" style="max-height: 250px;">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="images" class="col-md-4-left"><strong>{{__('Images: ')}}</strong></label>
                                <input id="images" type="file" name="images[]" onchange="readMultiFiles" multiple>
                            </div>
                            <div class="col-md-6">
                                <div class="row images-wrapper"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-3">
                        <button type="submit" class="btn btn-success">Add Product</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function (e) {
            $('#thumbnail').change(function(){
                let reader = new FileReader();
                reader.onload = (e) => {
                    $('#thumbnail-preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            });
        });

        if(window.FileReader) {
            document.getElementById('images').onchange = function() {
                let counter = -1, file;
                $('.images-wrapper').html('');
                let template = `<div class="col-sm-12 justify-content-center align-items-center">
                <img src="__url__" style="max-height: 250px;" ></div>`;

                while(file = this.files[++counter]) {
                    let reader = new FileReader();
                    reader.onloadend = (function () {
                        return function() {
                            let img = template.replace('__url__', this.result);
                            images += img;
                            $('.images-wrapper').append(img);
                        }
                    })(file);
                    reader.readAsDataURL(file);
                }
            }
        }
    </script>
@endsection




