@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3 class="text-center">{{ __('Products list') }}</h3>
        </div>
        <div class="col-md-12">
            <table class="table align-self-center">
                <thead class="text-center thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Thumbnail</th>
                    <th scope="col">Name</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Category</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody class="table-secondary">
                @foreach( $products as $product)
                <tr>
                    <td class="text-center">{{ $product->id }}</td>
                    <td class="text-center">
                        @if(Storage::has($product->thumbnail))
                            <img src="{{ Storage::url($product->thumbnail) }}" width="75" height="95" alt="{{ $product->title }}">
                        @else
                            <img src="{{ $product->thumbnail }}" width="75" height="95" alt="{{ $product->title }}">
                        @endif
                    </td>
                    <td class="text-center">{{ $product->title }}</td>
                    <td class="text-center">{{ $product->in_stock }}</td>
                    <td class="text-center">
                        @include('categories.parts.category_view', ['category' => $product->category])
                    </td>
                    <td class="text-center">
                        <a href="{{ route('products.show', $product) }}" class="btn btn-outline-secondary">View</a>
                        <a href="{{ route('admin.products.edit', $product) }}" class="btn btn-outline-success">Edit</a>
                        <form action="{{ route('admin.products.delete', $product) }}" class="d-inline-block " method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-outline-danger" value="Remove" >
                        </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-2">
            {{ $products->links() }}
        </div>
    </div>
</div>
@endsection
