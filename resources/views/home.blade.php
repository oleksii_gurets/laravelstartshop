@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="mb-3"><span class="text-muted">Product categories:</span>
                @each('categories.parts.category_view', $categories, 'category')
            </div>
            <div class="d-flex justify-content-around">
                @each('products.parts.product_view', $products, 'product')
            </div>

{{--        <div class="card">
               <div class="card-header">{{ __('Dashboard') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{ __('You are logged in!') }}
                </div>
            </div>
--}}   </div>
    </div>
</div>
@endsection
