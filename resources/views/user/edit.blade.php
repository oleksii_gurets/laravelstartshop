@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('user.parts.user-nav')
            </div>
            <div class="col-md-12">
                <h3 class="text-center">{{ __('Edit Profile Data') }}</h3>
                <hr>
            </div>
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-8">
                <form method="POST" action="{{ route('user.update', $user) }}">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-6">
                            <input id="role_id"
                                   type="hidden"
                                   class="form-control @error('role_id') is-invalid @enderror"
                                   name="role_id"
                                   value="{{ $user->role_id }}"
                                   autocomplete="role_id"
                                   autofocus>
                            <input id="name"
                                   type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name" value="{{ $user->name }}"
                                   autocomplete="name"
                                   autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Surname') }}</label>
                        <div class="col-md-6">
                            <input id="surname"
                                   type="text"
                                   class="form-control @error('surname') is-invalid @enderror"
                                   name="surname"
                                   value="{{$user->surname }}"
                                   autocomplete="surname"
                                   autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                        <div class="col-md-6">
                            <input id="email"
                                   type="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   name="email"
                                   value="{{ $user->email }}"
                                   autocomplete="email">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>
                        <div class="col-md-6">
                            <input id="phone_number"
                                   type="tel"
                                   class="form-control @error('phone_number') is-invalid @enderror"
                                   name="phone_number"
                                   value="{{ $user->phone_number }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="birthday" class="col-md-4 col-form-label text-md-right">{{ __('Birthday') }}</label>
                        <div class="col-md-6">
                            <input id="birthday_date"
                                   type="date"
                                   class="form-control @error('birthday_date') is-invalid @enderror"
                                   name="birthday_date"
                                   value="{{ $user->birthday_date }}">
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-success">
                                {{ __('Update data') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
