@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 class="text-center">{{ __('Wish List') }}</h3>
                <hr>
            </div>
            <div class="col-md-12">
                @if(Cart::instance('wishlist')->count() > 0)
                    <table class="table table-light table-bordered text-center">
                        <thead>
                        <tr>
                            <th class="border border-info">Thumbnail</th>
                            <th class="border border-info">Product</th>
                            <th class="border border-info">Price</th>
                            <th class="border border-info"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach(Cart::instance('wishlist')->content() as $wishItem)
                                <tr>
                                    <td class="border border-info">
                                        @if(Storage::has($wishItem->model->thumbnail))
                                            <img src="{{ Storage::url($wishItem->model->thumbnail) }}" alt="{{ $wishItem->name }}" style="width: 50px;">
                                        @else
                                            <img src="{{ $wishItem->model->thumbnail }}" alt="{{ $wishItem->name }}" style="width: 50px;">
                                        @endif
                                    </td>
                                    <td class="border border-info">
                                        <a href="{{ route('products.show', $wishItem->id) }}"><strong>{{ $wishItem->name }}</strong></a>
                                    </td>
                                    <td class="border border-info">
                                        {{ $wishItem->price }}$
                                    </td>
                                    <td class="border border-info">
                                        <form action="{{ route('wishlist.delete', $wishItem->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="hidden" name="rowId" value="{{ $wishItem->rowId }}">
                                            <input type="submit" class="btn btn-danger" value="Remove">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <h3 class="text-center">There are no products in your wish list</h3>
                @endif
            </div>
        </div>
    </div>
@endsection
