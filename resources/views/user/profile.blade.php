@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('user.parts.user-nav')
            </div>
            <div class="col-md-8">
                <div class="mb-3"><span class="text-muted">{{ __('User profile:') }}</span>
                    Hello, {{ $user->name . " " . $user->surname }} !
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-secondary table-borderless table-sm">
                    <thead class="table-info">
                    <tr>
                        <th class="text-center" scope="col"> Name </th>
                        <th class="text-center" scope="col"> Value <th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td scope="row" class="text-center">{{__('Role')}}</td>
                        <td class="text-center"> {{ $user->role->name }} </td>
                    </tr>
                    <tr>
                        <td scope="row" class="text-center">{{__('Name')}}</td>
                        <td class="text-center"> {{ $user->name }} </td>
                    </tr>
                    <tr>
                        <td scope="row" class="text-center">{{__('Surname')}}</td>
                        <td class="text-center"> {{ $user->surname }} </td>
                    </tr>
                    <tr>
                        <td scope="row" class="text-center">{{__('E-Mail')}}</td>
                        <td class="text-center"> {{ $user->email }} </td>
                    </tr>
                    <tr>
                        <td scope="row" class="text-center">{{__('Birthday')}}</td>
                        <td class="text-center"> {{ $user->birthday_date }} </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
