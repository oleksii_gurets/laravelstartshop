@extends('layouts.app')
@inject('wishlist', 'App\Services\WishlistService')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 class="text-center"><span class="badge badge-secondary">{{ __($product->title) }}</span></h3>
            </div>
        </div>
        <hr class="shadow-lg mb-1 bg-white rounded">
        <div class="row mt-3">
            <div class="col-md-4">
                @if(Storage::has($product->thumbnail))
                    <img src="{{ Storage::url($product->thumbnail) }}" class="card-img-top" style="width: 250px; height: 300px" alt="{{ $product->title }}">
                @else
                    <img src="{{ $product->thumbnail }}" class="card-img-top" style="width: 250px; height: 300px" alt="{{ $product->title }}">
                @endif
            </div>
            <div class="col-md-6">
                <h5>
                    <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">Product price: </span>
                    @if(isset($product->discount))
                        <s>{{$product->price}}$</s>
                        <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">Discount price: </span>
                        {{$product->getPrice()}}$
                    @else
                        {{$product->price}}$
                    @endif
                </h5>
                <h5>
                    <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">Product SKU: </span> {{ $product->SKU }}
                </h5>
                <h5>
                    <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">In stock: </span> {{ $product->in_stock }}
                </h5>
                <h5>
                    <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">Product category: </span>
                    {{ $product->category->name }}
                </h5>
                <h5>
                    <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">
                        <a href="{{ route('products.images', $product->id) }}">Product gallery</a>
                    </span>
                    @auth
                    @if($product->in_stock > 0)
                    <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">
                        <form action="{{ route('cart.add', $product) }}" class="form-inline" method="POST">
                            @csrf
                            @method('post')
                            <input type="hidden" name="discount_price" value="">
                            <label for="product_count" class="sr-only">Count: </label>
                            <input type="number" name="product_count"
                            class="form-control" id="product_count" min="1" max="{{ $product->in_stock }}" value="1">
                            <button type="submit" class="btn btn-success ml-2">Add to cart</button>
                        </form>
                    </span>
                    @endif
                        @if($wishlist->isUserFollowed($product))
                            <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">
                                <form action="{{ route('wishlist.delete', $product) }}" class="form-inline" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger" value="Remove from Wish List">
                                </form>
                            </span>
                        @else
                            <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">
                                <a href="{{ route('wishlist.add', $product) }}"
                                   class="btn btn-success">{{ __('Add to Wish List') }}</a>
                            </span>
                        @endif
                    @endauth
                </h5>
            </div>
        </div>
        <hr class="shadow-lg p-1 mb-1 bg-white rounded">
        <div class="col-md-12">
            <h5>
                <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">Product description: </span>
            </h5>
            <p>{{ ($product->description) }}</p>
        </div>
        <div class="col-md-12">
            <h4>
                <a href="{{route('products') }}" class="badge badge-info">Back to product-list</a>
            </h4>
        </div>
    </div>
@endsection
