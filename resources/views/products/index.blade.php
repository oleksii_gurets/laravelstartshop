@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3 class="text-center">{{ __('Products list') }}</h3>
        </div>
        <div class="col-md-12">
            <div class="container">
                 <div class="row">
                     @each('products.parts.product_view', $products, 'product')
                 </div>
            </div>
        </div>
        <div class="col-md-2">
            {{ $products->links() }}
        </div>
    </div>
</div>
@endsection

