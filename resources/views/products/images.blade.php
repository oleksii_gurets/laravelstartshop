@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <h3 class="text-center"><span class="badge badge-secondary">{{ __($product->title) }}{{__(' gallery') }}</span></h3>
        </div>
        <div id="product-carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach($product->gallery as $image)
                    @if(Storage::has($image->path))
                        <li data-target="#product-carousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                    @endif
                @endforeach
            </ol>
            <div class="carousel-inner" style="width:100%; height: 480px !important;">
                @foreach($product->gallery as $image)
                    @if(Storage::has($image->path))
                        <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                            <img src="{{ Storage::url($image->path) }}" class="d-block w-100" alt="...">
                        </div>
                    @endif
                @endforeach
            </div>
            <style>
                .carousel-control-next-icon,
                .carousel-control-prev-icon {
                    filter: invert(1);
                }
            </style>
            <a class="carousel-control-prev" href="#product-carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#product-carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="col-md-12 m-5">
            <h4>
                <a href="{{ route('products.show', $product->id) }}" class="badge badge-info">Back to product</a>
            </h4>
        </div>
    </div>
@endsection
