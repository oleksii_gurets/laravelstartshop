<div class="col-md-4 mb-3">
    <div class="card shadow mb-5 bg-white rounded">
        <div class="card-header">
            <h5>Product name:</h5>
            {{ $product->title }}
        </div>
        @if(Storage::has($product->thumbnail))
            <img src="{{ Storage::url($product->thumbnail) }}" class="card-img-top col-md-auto"  alt="{{ $product->title }}">
        @else
            <img src="{{ $product->thumbnail }}" class="card-img-top col-md-auto"  alt="{{ $product->title }}">
        @endif
        <div class="card-body">
            <h5>Product description:</h5>
            <p class="m-2">{{ $product->short_description }}</p>
            @if(isset($product->discount))
                <h5>Price: <s>{{$product->price}}$</s></h5>
                <h5>Discount price: {{$product->getPrice()}}$</h5>
            @else
                <h5>Price: {{$product->price}}$</h5>
            @endif
            <a href="{{ route('products.show', $product->id) }}" class="btn btn-primary">Show</a>
        </div>
    </div>
</div>

