@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 class="text-center">{{ __('Cart') }}</h3>
                <hr>
            </div>
            <div class="col-md-12">
                @if(Cart::instance('cart')->count() > 0)
                <table class="table align-self-center">
                    <thead class="text-center thead-dark">
                    <tr>
                        <th scope="col">Product</th>
                        <th scope="col">Qty</th>
                        <th scope="col">Price</th>
                        <th scope="col">Subtotal</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="table-secondary">
                    @foreach( Cart::content() as $row)
                        <tr>
                            <td class="text-info text-center"><a href="{{ route('products.show', $row->id) }}">{{ $row->name }}</a></td>
                            <td class="text-center">
                                <form action="{{ route('cart.count.update', $row->id) }}" method="POST">
                                    @csrf
                                    <input type="hidden" value="{{ $row->rowId }}" name="rowId">
                                    <input type="number" min="1" value="{{ $row->qty }}" name="product_count">
                                    <input type="submit" class="btn btn-outline-success" value="Update count">
                                </form>
                            </td>
                            <td class="text-center">${{ $row->price }}</td>
                            <td class="text-center">${{ $row->subtotal }}</td>
                            <td class="text-center">
                                <form action="{{ route('cart.delete') }}" method="POST">
                                    @csrf
                                    <input type="hidden" value="{{ $row->rowId }}" name="rowId">
                                    <input type="submit" class="btn btn-outline-danger" value="Remove" >
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                    <h3 class="text-danger text-center">Your cart is empty, go to
                        <a class="navbar-brand" href="{{ url('/products-list') }}">
                            <h3>products-list</h3>
                        </a>
                        to make purchase!
                    </h3>
                @endif
            </div>
        </div>
        @if(Cart::instance('cart')->count() > 0)
            <div class="row justify-content-end">
                <div class="col-md-4">
                    <div class="table-responsive">
                        <table class="table table-dark">
                            <tbody>
                            <tr>
                                <td>Subtotal</td>
                                <td>${{ Cart::subtotal() }}</td>
                            </tr>
                            <tr>
                                <td>Tax</td>
                                <td>${{ Cart::tax() }}</td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>${{ Cart::total() }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12 text-right">
                    <a href="{{ route('checkout') }}" class="btn btn-outline-success">{{__('Proceed to checkout') }}</a>
                </div>
            </div>
        @endif
    </div>
@endsection
