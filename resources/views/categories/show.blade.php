@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 class="text-center">{{ __('Category: ') }}{{ '"' . $category->name . '"' }}</h3>
            </div>
        </div>
        <hr class="shadow-lg mb-1 bg-white rounded">
        <div class="col-md-12 text-center">
            <h5>
                <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">{{__('Category description: ')}}</span>
            </h5>
            <p>{{ ($category->description) }}</p>
        </div>
        <hr class="shadow-lg mb-1 bg-white rounded">
        <div class="col-md-12 text-center">
            <h5>
                <span class="badge badge-pill badge-light shadow p-3 mb-2 bg-white rounded">{{__('Category products: ')}}</span>
            </h5>
        </div>
        <div class="col-md-12">
            <div class="container">
                <div class="row">
                    @each('products.parts.product_view', $products, 'product')
                </div>
            </div>
        </div>
        <div class="col-md-2">
            {{ $products->links() }}
        </div>
        <div class="col-md-12">
            <h4>
                <a href="{{route('categories') }}" class="badge badge-info">Back to categories</a>
            </h4>
        </div>
    </div>
@endsection
