@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 class="text-center">{{ __('Product categories') }}</h3>
            </div>
            <div class="col-md-12">
                <div class="container">
                    <table class="table text-center">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Category name</th>
                                <th scope="col">Products quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>@include('categories.parts.category_view', ['category' => $category])</td>
                                <td>{{ $category->products->count()}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-2">
                {{ $categories->links() }}
            </div>
        </div>
    </div>
@endsection
