<a href="{{ route('categories.show', $category->id) }}" style="font-size:large" class="m-2 badge badge-dark">
    {{ __($category->name) }}
</a>
