<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateProductRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->check() && Auth::user()->role()->first()->isAdmin();
    }

    public function messages()
    {
        return [
            'title.required' => 'The product must be provided with title',
            'title.unique' => 'Title should be unique',
            'title.min' => 'Title should contain more than 5 symbols',
            'discount.numeric' => 'Discount must be integer',
            'in_stock.numeric' => 'In stock must be integer'
        ];
    }

    public function rules()
    {
        return [
            'title' => ['required', 'string', 'min:5', 'unique:products'],
            'description' => ['required', 'string', 'min:20'],
            'short_description' => ['required', 'string', 'min:20', 'max:150'],
            'SKU' => ['required', 'min:2', 'unique:products'],
            'price' => ['required', 'numeric', 'min:1'],
            'discount' => ['required', 'numeric', 'max:90'],
            'in_stock' => ['required', 'numeric'],
            'thumbnail' => ['required', 'image: jpeg, png'],
            'images.*' => ['image: jpeg, png'],
            'category' => ['required', 'numeric']
        ];
    }
}
