<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => ['required', 'string', 'min:2'],
            "surname" => ['required', 'string', 'min:3'],
            "email" => ['required', 'email'],
            "phone_number" => ['required', 'string', 'min:2'],
            "birthday_date" => ['required', 'date', 'min:2'],
        ];
    }
}
