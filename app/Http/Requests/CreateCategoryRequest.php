<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateCategoryRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->check() && Auth::user()->role()->first()->isAdmin();
    }

    public function messages()
    {
        return [
            'name.required' => 'The category must be provided with name',
            'name.unique' => 'Category name should be unique'
        ];
    }


    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:50', 'unique:categories']
        ];
    }
}
