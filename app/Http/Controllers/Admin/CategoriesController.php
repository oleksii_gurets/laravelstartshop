<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCategoryRequest;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::withCount('products')->paginate(6);
        return view('admin/categories/index', compact('categories'));
    }


    public function create()
    {
        return view('admin/categories/create');
    }


    public function store(CreateCategoryRequest $request)
    {
        Category::create($request->all());
        return redirect()->route('admin.categories')
            ->with('status','Category created successfully.');
    }


    public function edit(Category $category)
    {
        return view('admin/categories/edit',compact('category'));
    }

    public function update(CreateCategoryRequest $request, Category $category)
    {
        $category->update($request->all());
        return redirect()->route('admin.categories')
            ->with('status','Category updated successfully');
    }

    //Remove action from admin panel category list
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->route('admin.categories')
            ->with('status','Category deleted successfully');
    }
}
