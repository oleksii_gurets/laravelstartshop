<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Services\ProductImagesService;
use Illuminate\Http\Request;
use App\Http\Requests\CreateProductRequest;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::with('category')->paginate(10);

        return view('admin/products/index', compact('products'));
    }

    public function create()
    {
        $categories = Category::all();

        return view('admin/products/create', compact('categories'));
    }


    public function store(CreateProductRequest $request)
    {
        $fields = $request->validated();

        $category = Category::find($fields['category']);

        $images = !empty($fields['images']) ? $fields['images'] : [];

        $product = $category->products()->create($fields);

        ProductImagesService::attach($product, $images);

        return redirect()->route('admin.products')
            ->with('status','Product created successfully.');
    }


    public function edit(Product $product)
    {
        $categories = Category::all(['id', 'name'])->toArray();

        return view('admin.products.edit',compact('product', 'categories'));
    }



    public function update(UpdateProductRequest $request, Product $product)
    {
        $updateData = $request->validated();
        //dd($updateData['thumbnail']);
        if(is_null($updateData['thumbnail'])) {
            unset($updateData['thumbnail']);
        }
        $product->update($updateData);

        if(!empty($request->images)) {
            ProductImagesService::attach($product, $request->images);
        }

        return redirect()->route('admin.products')
            ->with('status', "Product {$product->id} updated successfully");
    }

    public function destroy(Product $product)
    {
        $product->orders()->detach();

        $images = $product->gallery()->get();
        if($images->count() > 0) {
            $images->each->delete();
        }

        $product->delete();
        return redirect()->route('admin.products')
            ->with('status', "Product {$product->id} was deleted successfully");
    }
}
