<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserAccountController extends Controller
{

    public function index()
    {
        $user = User::with('role')->find(auth()->user()->id);
        return view('user/profile', compact('user'));
    }

    public function edit(User $user)
    {
        return view('user/edit', compact('user'));
    }


    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update($request->validated());
        return redirect(route('user.profile'))->with('status', 'Profile data was successfully updated!');
    }

    /*public function show(Request $request)
    {
        $user = Auth::user();

        return view('user/profile', compact( 'user'));

    }*/

}
