<?php

namespace App\Services;


use App\Models\Product;
use App\Services\Contracts\WishlistServiceInterface;

class WishlistService implements WishlistServiceInterface
{
    public function isUserFollowed(Product $product)
    {
        $followers = $product->userFollow()->get()->pluck('id');

        return $followers->contains(auth()->id());
    }
}
