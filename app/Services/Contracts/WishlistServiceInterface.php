<?php

namespace App\Services\Contracts;

use App\Models\Product;

interface WishlistServiceInterface
{
    public function isUserFollowed(Product $product);
}
