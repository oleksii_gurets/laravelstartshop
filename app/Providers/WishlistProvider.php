<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class WishlistProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Services\Contracts\WishlistServiceInterface::class,
            \App\Services\WishlistService::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
