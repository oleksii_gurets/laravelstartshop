<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'surname',
        'birthday_date',
        'email',
        'phone_number',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function instanceCartName()
    {
        $userName = [
            $this->id,
            $this->surname
        ];

        return implode('_', $userName);
    }

    public function wish()
    {
        return $this->belongsToMany(
            \App\Models\Product::class,
            'wishlist',
            'user_id',
            'product_id'
        );
    }

    public function addToWish(Product $product)
    {
        return $this->wish()->attach($product);
    }

    public function removeFromWish(Product $product)
    {
        return $this->wish()->detach($product);
    }
}

