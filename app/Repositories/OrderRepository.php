<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderStatus;
use App\Repositories\Contracts\OrderRepositoryInterface;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class OrderRepository implements OrderRepositoryInterface
{
    public function create(Request $request): Order
    {
        $status = OrderStatus::where('name', '=', Config::get('constants.db.order_status.in_process'))->first();
        $orderData = $request->validated();

        $orderData['status_id'] = $status->id;
        $orderData['total'] = Cart::instance('cart')->total(2, '.', '');

        $cartItems = Cart::instance('cart')->content()->groupBy('id');
        $order = auth()->user()->orders()->create($orderData);

        $cartItems->each(function($item) use ($order) {
            $product = $item[0];
            $order->products()->attach(
                $product->model,
                [
                    'quantity' => $product->qty,
                    'single_price' => $product->model->getPrice()
                ]
            );
            $product->model->in_stock -= $product->qty;
            $product->model->update();
        });
        return $order;
    }
}
